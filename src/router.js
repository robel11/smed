import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/pages',
      component: () => import('@/views/patients/pages/Index'),
      children: [{
          name: 'Lock',
          path: 'lock',
          component: () => import('@/views/patients/pages/Lock')
        },
        {
          name: 'Login',
          path: 'login',
          component: () => import('@/views/patients/pages/Login')
        },
        {
          name: 'Pricing',
          path: 'pricing',
          component: () => import('@/views/patients/pages/Pricing')
        },
        {
          name: 'Register',
          path: 'register',
          component: () => import('@/views/patients/pages/Register')
        }
      ]
    },
    {
      path: '/',
      component: () => import('@/views/patients/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/patients/dashboard/Dashboard')
        },
        // Pages
        {
          name: 'Diagnosis',
          path: 'patient/diagnosis',
          component: () => import('@/views/patients/dashboard/Diagnosis')
        },
        {
          name: 'Daily Tips',
          path: 'patient/daily_tips',
          component: () => import('@/views/patients/dashboard/DailyTips')
        },
        {
          name: 'Daily Record',
          path: 'patient/daily_record',
          component: () => import('@/views/patients/dashboard/DailyRecord')
        },
        {
          name: 'Messenger Home',
          path: 'patient/messenger_home',
          component: () => import('@/views/patients/dashboard/MessengerHome')
        },
        {
          name: 'Edit Profile',
          path: 'patient/edit_profile',
          component: () => import('@/views/patients/dashboard/EditProfile')
        },
        {
          name: 'User Profile',
          path: 'pages/user',
          component: () => import('@/views/patients/dashboard/pages/UserProfile')
        },
        {
          name: 'Find Doctor',
          path: '/find_doctors',
          component: () => import('@/views/patients/dashboard/FindDoctor')
        },
        {
          name: 'My Appointment',
          path: '/my_appointment',
          component: () => import('@/views/patients/dashboard/MyAppointment')
        },
        {
          name: 'My Prescription',
          path: '/my_prescription',
          component: () => import('@/views/patients/dashboard/MyPrescription')
        },
        // {
        //   name: 'Timeline',
        //   path: 'pages/timeline',
        //   component: () => import('@/views/patients/dashboard/pages/Timeline')
        // },
        // Components
        // {
        //   name: 'Buttons',
        //   path: 'components/buttons',
        //   component: () => import('@/views/patients/dashboard/component/Buttons')
        // },
        // {
        //   name: 'Grid System',
        //   path: 'components/grid-system',
        //   component: () => import('@/views/patients/dashboard/component/Grid')
        // },
        // {
        //   name: 'Tabs',
        //   path: 'components/tabs',
        //   component: () => import('@/views/patients/dashboard/component/Tabs')
        // },
        // {
        //   name: 'Notifications',
        //   path: 'components/notifications',
        //   component: () => import('@/views/patients/dashboard/component/Notifications')
        // },
        // {
        //   name: 'Icons',
        //   path: 'components/icons',
        //   component: () => import('@/views/patients/dashboard/component/Icons')
        // },
        // {
        //   name: 'Typography',
        //   path: 'components/typography',
        //   component: () => import('@/views/patients/dashboard/component/Typography')
        // },
        // Forms
        // {
        //   name: 'Regular Forms',
        //   path: 'forms/regular',
        //   component: () => import('@/views/patients/dashboard/forms/RegularForms')
        // },
        // {
        //   name: 'Extended Forms',
        //   path: 'forms/extended',
        //   component: () => import('@/views/patients/dashboard/forms/ExtendedForms')
        // },
        // {
        //   name: 'Validation Forms',
        //   path: 'forms/validation',
        //   component: () => import('@/views/patients/dashboard/forms/ValidationForms')
        // },
        // {
        //   name: 'Wizard',
        //   path: 'forms/wizard',
        //   component: () => import('@/views/patients/dashboard/forms/Wizard')
        // },
        // Tables
        // {
        //   name: 'Regular Tables',
        //   path: 'tables/regular-tables',
        //   component: () => import('@/views/patients/dashboard/tables/RegularTables')
        // },
        // {
        //   name: 'Extended Tables',
        //   path: 'tables/extended-tables',
        //   component: () => import('@/views/patients/dashboard/tables/ExtendedTables')
        // },
        // {
        //   name: 'Data Tables',
        //   path: 'tables/data-tables',
        //   component: () => import('@/views/patients/dashboard/tables/DataTables')
        // },
        // Maps
        // Root level
        // {
        //   name: 'Widgets',
        //   path: 'widgets',
        //   component: () => import('@/views/patients/dashboard/Widgets')
        // },
        // {
        //   name: 'Charts',
        //   path: 'charts',
        //   component: () => import('@/views/patients/dashboard/Charts')
        // },
        // {
        //   name: 'Calendar',
        //   path: 'calendar',
        //   component: () => import('@/views/patients/dashboard/Calendar')
        // }
      ]
    },
    {
      path: '*',
      component: () => import('@/views/patients/pages/Index'),
      children: [{
        name: '404 Error',
        path: '',
        component: () => import('@/views/patients/pages/Error')
      }]
    }
  ]
})
